﻿using GDGame;
using GDLibrary.Actors;
using GDLibrary.Containers;
using GDLibrary.Controllers;
using GDLibrary.Core.Controllers;
using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using GDLibrary.MyGame;
using GDLibrary.Parameters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace GDLibrary.Utilities
{
    /// <summary>
    /// Use the level loader to instanciate 3D drawn actors within your level from a PNG file.
    ///
    /// Usage:
    ///    LevelLoader levelLoader = new LevelLoader(this.objectArchetypeDictionary,
    ///    this.textureDictionary);
    ///     List<DrawnActor3D> actorList = levelLoader.Load(this.textureDictionary[fileName],
    ///           scaleX, scaleZ, height, offset);
    ///     this.object3DManager.Add(actorList);
    ///
    /// </summary>
    public class LevelLoader<T> where T : DrawnActor3D
    {
        private static readonly Color ColorLevelLoaderIgnore = Color.White;

        private Dictionary<string, T> archetypeDictionary;
        private ContentDictionary<Texture2D> textureDictionary;
        private bool direction = true;
        private KeyboardManager keyboardManager;
        private MouseManager mouseManager;
        private ObjectManager objectManager;
        private CameraManager<Camera3D> cameraManager;

        public CameraManager<Camera3D> CameraManager
        {
            get
            {
                return cameraManager;
            }
        }

        public LevelLoader(Dictionary<string, T> archetypeDictionary,
            ContentDictionary<Texture2D> textureDictionary, KeyboardManager keyboardManager, MouseManager mouseManager, ObjectManager objectManager, CameraManager<Camera3D> cameraManager)
        {
            this.archetypeDictionary = archetypeDictionary;
            this.textureDictionary = textureDictionary;

            this.keyboardManager = keyboardManager;
            this.mouseManager = mouseManager;
            this.objectManager = objectManager;
            this.cameraManager = cameraManager;
        }

        public List<DrawnActor3D> Load(Texture2D texture,
            float scaleX, float scaleZ, float height, Vector3 offset)
        {
            List<DrawnActor3D> list = new List<DrawnActor3D>();
            Color[] colorData = new Color[texture.Height * texture.Width];
            texture.GetData<Color>(colorData);

            Color color;
            Vector3 translation;
            DrawnActor3D actor;

            for (int y = 0; y < texture.Height; y++)
            {
                for (int x = 0; x < texture.Width; x++)
                {
                    color = colorData[x + y * texture.Width];

                    if (!color.Equals(ColorLevelLoaderIgnore))
                    {
                        //scale allows us to increase the separation between objects in the XZ plane
                        translation = new Vector3(x * scaleX, height, y * scaleZ);

                        //the offset allows us to shift the whole set of objects in X, Y, and Z
                        translation += offset;

                        actor = getObjectFromColor(color, translation);

                        if (actor != null)
                        {
                            list.Add(actor);
                        }
                    }
                } //end for x
            } //end for y
            return list;
        }

        private Random rand = new Random();

        private int count = 1;

        private DrawnActor3D getObjectFromColor(Color color, Vector3 translation)
        {
            ICollisionPrimitive collisionPrimitive = null;
            CollidablePrimitiveObject collidablePrimitiveObject = null;
            //if the pixel is red then draw a tall (stretched collidable unlit cube)
            if (color.Equals(new Color(255, 0, 0)))
            {
                PrimitiveObject archetype
                        = archetypeDictionary["lit textured pyramid"] as PrimitiveObject;

                PrimitiveObject drawnActor3D = archetype.Clone() as PrimitiveObject;

                //   PrimitiveObject drawnActor3D
                //       = archetypeDictionary["lit textured pyramid"].Clone() as PrimitiveObject;

                //change it a bit
                drawnActor3D.ID = "pyramid " + count++;
                drawnActor3D.Transform3D.Scale = 10 * new Vector3(3, 4, 1);
                drawnActor3D.EffectParameters.DiffuseColor = Color.Blue;
                drawnActor3D.EffectParameters.Alpha = 0.5f;
                drawnActor3D.Transform3D.Translation = translation;
                drawnActor3D.Transform3D.RotationInDegrees = new Vector3(0, 0, 0);
                return drawnActor3D;
            }
            else if (color.Equals(new Color(34, 177, 76))) //grass blocks
            {
                PrimitiveObject archetype = archetypeDictionary["lit textured cube"] as PrimitiveObject;

                PrimitiveObject drawnActor3D = archetype.Clone() as PrimitiveObject;

                drawnActor3D.ID = "cube" + count++;
                drawnActor3D.Transform3D.Scale = 10 * new Vector3(1, 1, 1);
                drawnActor3D.Transform3D.Translation = translation;
                
                return drawnActor3D;
            }
            else if (color.Equals(new Color(127, 127, 127))) //asphalt blocks
            {
                PrimitiveObject archetype = archetypeDictionary["lit textured cube"] as PrimitiveObject;

                PrimitiveObject drawnActor3D = archetype.Clone() as PrimitiveObject;

                drawnActor3D.ID = "cube" + count++;
                drawnActor3D.EffectParameters.Texture = textureDictionary["Asphalt"];
                drawnActor3D.Transform3D.Scale = 10 * new Vector3(1, 1, 1);
                drawnActor3D.Transform3D.Translation = translation;

                return drawnActor3D;
            }
            else if(color.Equals(new Color(255, 127, 39))) //Win condition block
            {
                PrimitiveObject archetype = archetypeDictionary["lit textured cube"] as PrimitiveObject;

                PrimitiveObject drawnActor3D = archetype.Clone() as PrimitiveObject;

                drawnActor3D.ID = "cube" + count++;
                drawnActor3D.ActorType = Enums.ActorType.Win;
                drawnActor3D.EffectParameters.Texture = textureDictionary["WinBlock"];
                drawnActor3D.Transform3D.Scale = 10 * new Vector3(1, 1, 1);
                drawnActor3D.Transform3D.Translation = translation;

                collisionPrimitive = new BoxCollisionPrimitive(drawnActor3D.Transform3D);

                collidablePrimitiveObject = new CollidablePrimitiveObject(drawnActor3D.ID,
                    ActorType.Win, StatusType.Drawn | StatusType.Update, drawnActor3D.Transform3D,
                    drawnActor3D.EffectParameters, drawnActor3D.IVertexData, collisionPrimitive, objectManager);

                objectManager.Add(collidablePrimitiveObject);

                return null;
            }
            else if (color.Equals(new Color(237, 28, 36))) //Enemies
            {
                PrimitiveObject archetype = archetypeDictionary["lit textured pyramid"] as PrimitiveObject;

                PrimitiveObject drawnActor3D = archetype.Clone() as PrimitiveObject;

                drawnActor3D.ID = "Enemy" + count++;
                drawnActor3D.EffectParameters.Texture = textureDictionary["Enemy"];
                drawnActor3D.Transform3D.Scale = 10 * new Vector3(0.5f, 1, 0.5f);
                drawnActor3D.Transform3D.Translation = new Vector3(translation.X, translation.Y - 2, translation.Z);

                //drawnActor3D.ControllerList.Add(new RotationController("rot" + count, Enums.ControllerType.RotationOverTime, 2, new Vector3(1, 0.5f, 0)));

                Transform3DCurve transform3DCurve = new Transform3DCurve(CurveLoopType.Oscillate);

                if (direction == true)
                {
                    transform3DCurve.Add(new Vector3(translation.X, translation.Y, translation.Z), new Vector3(0, -90, 0), Vector3.UnitY, 3000);
                    transform3DCurve.Add(new Vector3(translation.X + 70, translation.Y, translation.Z), new Vector3(0, -90, 0), Vector3.UnitY, 6000);
                    
                    direction = false;
                }
                else
                {
                    transform3DCurve.Add(new Vector3(translation.X, translation.Y, translation.Z), new Vector3(0, -90, 0), Vector3.UnitY, 3000);
                    transform3DCurve.Add(new Vector3(translation.X - 70, translation.Y, translation.Z), new Vector3(0, -90, 0), Vector3.UnitY, 6000);

                    direction = true;
                }
                
                //transform3DCurve.Add(new Vector3(translation.X + 70, translation.Y, translation.Z), new Vector3(0, -90, 0), Vector3.UnitY, 2000);

                //drawnActor3D.ControllerList.Add(new Curve3DController("pan", ControllerType.Pan, transform3DCurve));

                collisionPrimitive = new BoxCollisionPrimitive(drawnActor3D.Transform3D);

                collidablePrimitiveObject = new CollidablePrimitiveObject(drawnActor3D.ID,
                    ActorType.Enemy, StatusType.Drawn | StatusType.Update, drawnActor3D.Transform3D,
                    drawnActor3D.EffectParameters, drawnActor3D.IVertexData, collisionPrimitive, objectManager);

                collidablePrimitiveObject.ControllerList.Add(new RotationController("rot" + count, Enums.ControllerType.RotationOverTime, 2, new Vector3(1, 0.5f, 0)));
                collidablePrimitiveObject.ControllerList.Add(new Curve3DController("pan", ControllerType.Pan, transform3DCurve));

                objectManager.Add(collidablePrimitiveObject);

                return null;
            }
            else if (color.Equals(new Color(255, 242, 0))) //Collectables
            {
                PrimitiveObject archetype = archetypeDictionary["lit textured octahedron"] as PrimitiveObject;

                PrimitiveObject drawnActor3D = archetype.Clone() as PrimitiveObject;

                drawnActor3D.ID = "Collectable" + count++;
                drawnActor3D.EffectParameters.Texture = textureDictionary["Collectable"];
                drawnActor3D.Transform3D.Scale = 5 * new Vector3(0.5f, 0.5f, 0.5f);
                drawnActor3D.Transform3D.Translation = new Vector3(translation.X-1, translation.Y, translation.Z+1);

                Transform3DCurve transform3DCurve = new Transform3DCurve(CurveLoopType.Oscillate);
                transform3DCurve.Add(new Vector3(drawnActor3D.Transform3D.Translation.X, drawnActor3D.Transform3D.Translation.Y + 1, drawnActor3D.Transform3D.Translation.Z), new Vector3(0, -90, 0), Vector3.UnitY, 1000);
                transform3DCurve.Add(new Vector3(drawnActor3D.Transform3D.Translation.X, drawnActor3D.Transform3D.Translation.Y - 1, drawnActor3D.Transform3D.Translation.Z), new Vector3(0, -90, 0), Vector3.UnitY, 2000);

                //drawnActor3D.ControllerList.Add(new Curve3DController("hover", Enums.ControllerType.Pan, transform3DCurve));

                collisionPrimitive = new BoxCollisionPrimitive(drawnActor3D.Transform3D);

                collidablePrimitiveObject = new CollidablePrimitiveObject(drawnActor3D.ID,
                    ActorType.CollidablePickup, StatusType.Drawn | StatusType.Update,
                    drawnActor3D.Transform3D, drawnActor3D.EffectParameters, drawnActor3D.IVertexData,
                    collisionPrimitive, objectManager);

                collidablePrimitiveObject.ControllerList.Add(new Curve3DController("hover", Enums.ControllerType.Pan, transform3DCurve));

                objectManager.Add(collidablePrimitiveObject);

                return null;
            }
            else if (color.Equals(new Color(163, 73, 164))) //Player
            {
                #region Player
                CollidablePlayerObject collidablePlayerObject = null;

                PrimitiveObject archetype = archetypeDictionary["lit textured cube"] as PrimitiveObject;

                PrimitiveObject drawnActor3D = archetype.Clone() as PrimitiveObject;

                drawnActor3D.ID = "Player";
                drawnActor3D.ActorType = Enums.ActorType.Player;
                drawnActor3D.EffectParameters.Texture = textureDictionary["Player"];
                drawnActor3D.Transform3D.Scale = 5 * new Vector3(1, 1, 1);
                drawnActor3D.Transform3D.Translation = new Vector3(translation.X, translation.Y - 2.5f, translation.Z);

                //drawnActor3D.ControllerList.Add(new FirstPersonController(GameConstants.Camera_NonCollidableFirstPerson, ControllerType.FirstPerson, keyboardManager, mouseManager, GameConstants.moveSpeed, GameConstants.strafeSpeed, GameConstants.rotateSpeed));

                collisionPrimitive = new BoxCollisionPrimitive(drawnActor3D.Transform3D);

                collidablePlayerObject = new CollidablePlayerObject(drawnActor3D.ID,
                    ActorType.CollidablePlayer,
                    StatusType.Drawn | StatusType.Update,
                    drawnActor3D.Transform3D,
                    drawnActor3D.EffectParameters,
                    drawnActor3D.IVertexData,
                    collisionPrimitive,
                    objectManager,
                    GameConstants.KeysOne,
                    GameConstants.moveSpeed,
                    GameConstants.rotateSpeed,
                    keyboardManager);

                objectManager.Add(collidablePlayerObject);
                #endregion Player

                #region Camera
                Transform3D transform3D = null;
                Camera3D camera3D = null;

                transform3D = new Transform3D(Vector3.Zero, -Vector3.UnitZ, Vector3.UnitY);

                camera3D = new Camera3D(GameConstants.Camera_CollidableThirdPerson,
                    ActorType.Camera3D, StatusType.Update, transform3D,
                    ProjectionParameters.StandardDeepSixteenTen,
                    new Viewport(0, 0, 1024, 768));

                camera3D.ControllerList.Add(new ThirdPersonController(GameConstants.Controllers_CollidableThirdPerson,
                    ControllerType.ThirdPerson,
                    collidablePlayerObject,
                    75,
                    40,
                    1,
                    mouseManager));

                object[] parameters = { camera3D };
                EventDispatcher.Publish(new EventData(EventCategoryType.Camera, EventActionType.AddCamera, parameters));
                cameraManager.Add(camera3D);
                #endregion Camera

                return null;
            }
            //add an else if for each type of object that you want to load...

            return null;
        }
    }
}