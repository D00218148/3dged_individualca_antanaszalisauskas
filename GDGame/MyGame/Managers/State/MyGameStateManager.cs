﻿using GDGame;
using GDLibrary.Actors;
using GDLibrary.Containers;
using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.GameComponents;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using GDLibrary.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace GDLibrary.Core.Managers.State
{
    /// <summary>
    /// Use this manager to listen for related events and perform actions in your game based on events received
    /// </summary>
    public class MyGameStateManager : PausableGameComponent, IEventHandler
    {
        private int score;
        private string level;
        ContentDictionary<Texture2D> textureDictionary;
        ObjectManager objectManager;
        LevelLoader<PrimitiveObject> levelLoader;
        List<DrawnActor3D> actorList;

        public int Score
        {
            get
            {
                return score;
            }
        }

        public string Level
        {
            get
            {
                return level;
            }
        }

        public MyGameStateManager(Game game, StatusType statusType, ContentDictionary<Texture2D> textureDictionary, ObjectManager objectManager, LevelLoader<PrimitiveObject> levelLoader) : base(game, statusType)
        {
            score = 0;
            level = "level1";
            this.textureDictionary = textureDictionary;
            this.objectManager = objectManager;
            this.levelLoader = levelLoader;
        }

        public override void SubscribeToEvents()
        {
            //add new events here...
            EventDispatcher.Subscribe(EventCategoryType.GameState, HandleEvent);

            base.SubscribeToEvents();
        }

        public override void HandleEvent(EventData eventData)
        {
            //add new if...else if statements to handle events here...
            if (eventData.EventActionType == EventActionType.LoadLevel)
            {
                actorList = levelLoader.Load(textureDictionary[eventData.Parameters[0] + ""], 10, 10, 20.5f, new Vector3(-50, 0, -150));
                objectManager.Add(actorList);

                actorList = levelLoader.Load(textureDictionary[eventData.Parameters[1] + ""], 10, 10, 30, new Vector3(-50, 0, -150));
                objectManager.Add(actorList);

                //object[] parameters = { levelLoader.CameraManager, level };
                //EventDispatcher.Publish(new EventData(EventCategoryType.Camera, EventActionType.PreviewLevel, parameters));
            }
            else if (eventData.EventActionType == EventActionType.OnPickup)
            {
                score += 10;
                object[] parameters = { score };
                EventDispatcher.Publish(new EventData(EventCategoryType.UI, EventActionType.OnApplyActionToFirstMatchActor,
                    (actor) => (actor as UITextObject).Text = "Score: " + score,
                    (actor) => actor.ActorType == ActorType.UIText && actor.ID.Equals("score"),
                    null));
            }
            else if (eventData.EventActionType == EventActionType.OnLose)
            {
                object[] parameters = { "deathsound" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnLose, null));
                //Game.Exit();
            }
            else if (eventData.EventActionType == EventActionType.NextLevel)
            {
                //EventDispatcher.Publish(new EventData(EventCategoryType.Level, EventActionType.ClearLevel, null));
                if (level.Equals("level1"))
                {
                    objectManager.ClearLevel();

                    levelLoader.CameraManager.RemoveFirstIf((camera) => { return camera.ID == GameConstants.Controllers_CollidableThirdPerson; });

                    level = "level2";

                    actorList = levelLoader.Load(textureDictionary[eventData.Parameters[0] + ""], 10, 10, 20.5f, new Vector3(-50, 0, -150));
                    objectManager.Add(actorList);

                    actorList = levelLoader.Load(textureDictionary[eventData.Parameters[1] + ""], 10, 10, 30, new Vector3(-50, 0, -150));
                    objectManager.Add(actorList);

                    object[] parameters = { levelLoader.CameraManager, level };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Camera, EventActionType.PreviewLevel, parameters));

                    //levelLoader.CameraManager.ActiveCameraIndex = 7;

                    
                }
                else
                {
                    object[] parameters = { "victorysound" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnWin, null));

                    //Game.Exit();
                }
            }
            //remember to pass the eventData down so the parent class can process pause/unpause
            base.HandleEvent(eventData);
        }

        protected override void ApplyUpdate(GameTime gameTime)
        {
            //add code here to check for the status of a particular set of related events e.g. collect all inventory items then...

            base.ApplyUpdate(gameTime);
        }
    }
}