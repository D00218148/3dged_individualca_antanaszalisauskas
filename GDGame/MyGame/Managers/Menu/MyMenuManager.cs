﻿using GDLibrary.Actors;
using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.Managers;
using GDLibrary.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDGame.MyGame.Managers
{
    public class MyMenuManager : MenuManager
    {
        private MouseManager mouseManager;
        private KeyboardManager keyboardManager;
        private LevelLoader<PrimitiveObject> levelLoader;

        public MyMenuManager(Game game, StatusType statusType, SpriteBatch spriteBatch,
            MouseManager mouseManager, KeyboardManager keyboardManager, LevelLoader<PrimitiveObject> levelLoader)
            : base(game, statusType, spriteBatch)
        {
            this.mouseManager = mouseManager;
            this.keyboardManager = keyboardManager;
            this.levelLoader = levelLoader;
        }

        public override void HandleEvent(EventData eventData)
        {
            if (eventData.EventCategoryType == EventCategoryType.Menu)
            {
                if (eventData.EventActionType == EventActionType.OnPause)
                    this.StatusType = StatusType.Drawn | StatusType.Update;
                else if (eventData.EventActionType == EventActionType.OnPlay)
                {
                    this.StatusType = StatusType.Off;

                    object[] parameters = { levelLoader.CameraManager, "level1" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Camera, EventActionType.PreviewLevel, parameters));
                }
                else if (eventData.EventActionType == EventActionType.OnLose)
                {
                    this.StatusType = StatusType.Drawn | StatusType.Update;
                    EventDispatcher.Publish(new EventData(EventCategoryType.Object, EventActionType.OnApplyActionToAllActors, (actor) => actor.StatusType = StatusType.Drawn, (actor) => actor.StatusType == StatusType.Update, null));

                    object[] parameters = { "backgroundmusic" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPause, parameters));
                    this.SetScene("lose");
                }
                else if(eventData.EventActionType == EventActionType.OnWin)
                {
                    this.StatusType = StatusType.Drawn | StatusType.Update;
                    EventDispatcher.Publish(new EventData(EventCategoryType.Object, EventActionType.OnApplyActionToAllActors, (actor) => actor.StatusType = StatusType.Drawn, (actor) => actor.StatusType == StatusType.Update, null));

                    object[] parameters = { "backgroundmusic" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPause, parameters));
                    this.SetScene("win");
                }
                    
            }
        }

        protected override void HandleInput(GameTime gameTime)
        {
            //bug fix - 7.12.20 - Exit button was hidden but we were still testing for mouse click
            if ((this.StatusType & StatusType.Update) != 0)
            {
                HandleMouse(gameTime);
            }

            HandleKeyboard(gameTime);
            //base.HandleInput(gameTime); //nothing happening in the base method
        }

        protected override void HandleMouse(GameTime gameTime)
        {
            foreach (DrawnActor2D actor in this.ActiveList)
            {
                if (actor is UIButtonObject)
                {
                    if (actor.Transform2D.Bounds.Contains(this.mouseManager.Bounds))
                    {
                        if (this.mouseManager.IsLeftButtonClickedOnce())
                        {
                            HandleClickedButton(gameTime, actor as UIButtonObject);
                        }
                    }
                }
            }
            base.HandleMouse(gameTime);
        }

        private void HandleClickedButton(GameTime gameTime, UIButtonObject uIButtonObject)
        {
            //benefit of switch vs if...else is that code will jump to correct switch case directly
            switch (uIButtonObject.ID)
            {
                case "play":
                    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPlay, null));

                    object[] parameters = { "backgroundmusic" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));
                    break;

                case "controlsandhelp":
                    this.SetScene("controlsandhelp");
                    break;

                case "back":
                    this.SetScene("main");
                    break;

                case "exit":
                    this.Game.Exit();
                    break;

                default:
                    break;
            }
        }

        protected override void HandleKeyboard(GameTime gameTime)
        {
            if (this.keyboardManager.IsFirstKeyPress(Microsoft.Xna.Framework.Input.Keys.M))
            {
                if (this.StatusType == StatusType.Off)
                {
                    //show menu
                    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPause, null));
                }
                else
                {
                    //show game
                    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPlay, null));
                }
            }

            base.HandleKeyboard(gameTime);
        }
    }
}